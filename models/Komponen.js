const mongoose = require("mongoose");

const komponenSchema = new mongoose.Schema({
  komponen: { type: String, required: true },
  sub: { type: String, required: true }
});

const Komponen = new mongoose.model("komponen", komponenSchema);

exports.Komponen = Komponen;
