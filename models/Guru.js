const mongoose = require("mongoose");

const guruSchema = new mongoose.Schema({
  nama: { type: String, required: true },
  nik: { type: String, required: true },
  mapel: { type: String, required: true }
});

const Guru = new mongoose.model("guru", guruSchema);

exports.Guru = Guru;
