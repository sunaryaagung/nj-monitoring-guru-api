const mongoose = require("mongoose");

penilaianSchema = new mongoose.Schema({
  guru: { type: mongoose.Schema.Types.ObjectId, ref: "gurus" },
  komponen: [
    {
      isi: { type: mongoose.Schema.Types.ObjectId, ref: "komponens" },
      nilai: Number,
      eval: String
    }
  ],
  skor: Number,
  tgl: { type: Date, default: Date.now() },
  semester: String,
  totalSkor: Number,
  evaluasi: String
});

const Penilaian = new mongoose.model("penilaians", penilaianSchema);

exports.Penilaian = Penilaian;
