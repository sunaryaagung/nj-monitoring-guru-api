const { Komponen } = require("../models/Komponen");

async function add(req, res) {
  try {
    const { komponen, sub } = req.body;
    const komponent = new Komponen({
      komponen,
      sub
    });

    await komponent.save();
    res.status(201).json({ data: komponent });
  } catch (err) {
    return res.status(422).json({ message: err.message });
  }
}

async function get(req, res) {
  const komponen = await Komponen.find();
  res.status(200).json({ data: komponen });
}

async function getId(req, res) {
  const kompo = await Komponen.findById(req.params.id);
  if (!kompo) return res.status(404).json({ message: "invlaid req" });

  res.status(200).json({ data: kompo });
}

async function update(req, res) {
  const komponen = await Komponen.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true }
  );
  if (!komponen) return res.status(404).json({ message: "invalid request" });
  res.status(200).json({ data: komponen });
}

async function del(req, res) {
  const komponen = await Komponen.findByIdAndRemove(req.params.id);
  if (!komponen) return res.stauts(404).json({ message: "invalid request" });
  res.status(200).json({ message: "deleted" });
}

module.exports = { add, get, getId, update, del };
