const { User } = require("../models/User");
const bcrypt = require("bcryptjs");

async function add(req, res) {
  const { username, password } = req.body;
  const user = new User({
    username,
    password
  });
  try {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    res.status(201).json({ data: user });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
}

async function del(req, res) {
  let user = await User.findByIdAndRemove(req.params.id);
  if (!user) return res.status(404).json({ message: "User not found" });
  res.status(200).json({ messag: "User deleted" });
}

async function login(req, res) {
  let { username, password } = req.body;
  let user = await User.findOne({ username: username });
  if (!user) return res.status(400).json({ message: "Wrong username" });

  let validPassword = await bcrypt.compare(password, user.password);
  if (!validPassword)
    return res.status(400).json({ message: "Wrong password" });

  const token = user.generateToken();
  res.status(200).json({ token: token });
}

module.exports = { add, del, login };
