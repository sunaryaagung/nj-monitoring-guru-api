const { Guru } = require("../models/Guru");

async function get(req, res) {
  const guru = await Guru.find();
  res.status(200).json({ data: guru });
}

async function getId(req, res) {
  const guru = await Guru.findById(req.params.id);
  if (!guru) return res.status(404).json({ message: "Invalid request" });
  res.status(200).json({ data: guru });
}

async function add(req, res) {
  try {
    const { nama, nik, mapel } = req.body;
    const guru = new Guru({
      nama,
      nik,
      mapel
    });
    await guru.save();
    res.status(201).json({ data: guru });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

async function update(req, res) {
  let guru = await Guru.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true }
  );
  if (!guru) return res.status(404).json({ message: "Invalid Request" });
  res.status(200).json({ data: guru });
}

async function del(req, res) {
  const guru = await Guru.findByIdAndRemove(req.params.id);
  if (!guru) return res.status(404).json({ message: "invalid request" });
  res.status(200).json({ message: "Deleted" });
}

module.exports = { get, getId, add, update, del };
