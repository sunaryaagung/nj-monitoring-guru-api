const { Penilaian } = require("../models/Penilaian");
const { Guru } = require("../models/Guru");
const { komponen } = require("../models/Komponen");

async function get(req, res) {
  const penilaian = await Penilaian.find();
  res.status(200).json({ data: penilaian });
}

async function getId(req, res) {
  const penilaian = await Penilaian.findById(req.params.id);
  if (!penilaian) return res.status(404).json({ message: "invalid request" });

  res.status(200).json({ data: penilaian });
}

async function add(req, res) {
  try {
    const penilaian = new Penilaian({
      guru: req.body.guru,
      komponen: [...req.body.komponen],
      semester: req.body.semester,
      totalSkor: 0,
      evaluasi: req.body.evaluasi
    });
    let total = 0;
    penilaian.komponen.forEach(element => {
      total = total + element.nilai;
    });
    penilaian.totalSkor = total;
    await penilaian.save();
    res.status(201).json({ data: penilaian });
  } catch (err) {
    res.status(422).json({ message: err.message });
  }
}

async function del(req, res) {
  const komponen = await Penilaian.findByIdAndRemove(req.params.id);
  if (!komponen) return res.status(404).json({ message: "invalid request" });
  res.status(200).json({ message: "Deleted" });
}

module.exports = { get, add, del, getId };
