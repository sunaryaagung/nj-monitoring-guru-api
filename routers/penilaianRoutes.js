const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const { get, getId, add, del } = require("../controllers/penilaianCont");

router.get("/", auth, get);
router.post("/", auth, add);
router.delete("/:id", auth, objectId, del);
router.get("/:id", auth, objectId, getId);

module.exports = router;
