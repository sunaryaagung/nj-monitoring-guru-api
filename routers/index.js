const express = require("express");
const router = express.Router();
const user = require("./userRoutes");
const guru = require("./guruRoutes");
const komponen = require("./komponenRoutes");
const penilaian = require("./penilaianRoutes");

router.use("/users", user);
router.use("/guru", guru);
router.use("/komponen", komponen);
router.use("/penilaian", penilaian);

module.exports = router;
