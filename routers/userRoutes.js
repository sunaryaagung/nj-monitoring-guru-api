const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const { add, del, login } = require("../controllers/userCont");

router.post("/", add);
router.delete("/:id", auth, objectId, del);
router.post("/login", login);

module.exports = router;
