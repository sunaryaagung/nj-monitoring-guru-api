const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const { get, getId, add, update, del } = require("../controllers/guruCont");

router.get("/", auth, get);
router.get("/:id", auth, objectId, getId);
router.post("/", auth, add);
router.put("/:id", auth, objectId, update);
router.delete("/:id", auth, objectId, del);

module.exports = router;
