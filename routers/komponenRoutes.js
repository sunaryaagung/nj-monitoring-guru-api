const express = require("express");
const router = express.Router();
const { add, get, getId, update, del } = require("../controllers/komponenCont");

router.post("/", add);
router.get("/", get);
router.get("/:id", getId);
router.put("/:id", update);
router.delete("/:id", del);

module.exports = router;
