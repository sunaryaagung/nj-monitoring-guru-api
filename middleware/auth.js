const { User } = require("../models/User");
const jwt = require("jsonwebtoken");

module.exports = async function(req, res, next) {
  let token = req.header("Authorization");
  if (!token) return res.status(401).json({ message: "Login pls" });

  try {
    let decoded = jwt.verify(token, process.env.SECRET_KEY);
    req.user = decoded;
    let user = await User.findById(req.user._id);
    if (!user) return res.status(401).json({ message: "Invalid token" });

    next();
  } catch (err) {
    res.status(422).json({ message: err.message });
  }
};
